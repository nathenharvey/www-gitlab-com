---
layout: markdown_page
title: "Secret Snowflake"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Secret Snowflake
We will be organizing a **Secret Snowflake** every year!

How does this work?

1. Join `#secret-snowflake` in Slack by mid-November of that year and give a thumbsup.

2. You'll receive an email to enroll. Click on the link to be taken to the app where you can list your wishes. 

**Avoid the ads, you don't need to click further to login**

3. Enter your address (please add this as a wishlist item with the format Address: " "), and then find out who you will ship a present to. 

4. Get a present, and ship it!   
**budget: €20 (does not include shipping)**

5. Finally! It arrived! Unpack it, take a picture, and thank your match in the `#secret-snowflake` channel on Slack to let them know that the gift has been received!

6. Perfectly content, you wait until next year to participate again.

**Tips:**

- It is also helpful to add a website or link to your wishlist that allows for easy/cost effective international shipping. If you have any issues shipping the present to your buddy, feel free to reach out to People Ops in Slack to assist in finding a local retailer.
  
- If you have not received your gift by January 15th, or are unsure if your match received the gift you sent, please reach out to People Ops.

*Note: By participating in Secret Snowflake you grant your match access to your address via People Ops.*

### Organizing Secret Snowflake

1. Create a `#secret-snowflake-(year)` channel 

1. Announce the deadline on the company call and in the `#company-announcements` channel with a link to this page as well as a disclaimer that this activity gives People Ops the right to share your address with your match.

**After the November deadline**  

1. Use [an organizer platform](https://www.secretsantaorganizer.com/) to automatically create matches and send emails.

1. Test the platform before sending out all emails to participants.

1. Add emails and create matches

*During this time, make sure to follow up any issues that may arise.*

**After January 15**  

Ensure all participants received their present.

#### F.A.Q.
**1. Can I go over budget?**  

No. Maybe €2.

**2. Can I go under budget?**  

That is perfectly fine. It is more important to give something awesome than to spend € 20.

**3. What if I have to ship something to the other side of the world?**  

You don't have to, there are eCommerce apps/websites in most countries. Let them do the heavy lifting! If you can't come up with a solution, please ask @zj on Slack.

**4. Shipping my trebuchet will be expensive, can I take it with me to the next summit and give it there?**  

For a full size trebuchet I'll make an exception, other than that, please do not bring your gift to the next summit. This means a couple of things:

1. The other participant has to have some spare room in their suitcase
1. Some people will already have a gift, I wouldn't be able to handle the suspense!
1. No clue, just ship it!

**5. Can I add a poem to the package?**

So, if you're referring to the good old [Sinterklaasgedichten](https://en.wikipedia.org/wiki/Sinterklaas), yes! You can and should! Just keep in mind not everyone is familiar with tone usually used in them.

**6. Cool idea, but I'd rather not be a participant this year.**

That is ok! No hard feelings, and maybe next year
