---
layout: markdown_page
title: "YouTube"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Post everything

We post everything that doesn't contain confidential information to YouTube.
We found that it helps with:

1. **Hiring** because candidates can get a feel for the company by seeing meetings.
1. **Retention** because sharing reinforces our value of transparency.
1. **Community** because people feel more part of what is happening at the company.
1. **Sales** because people see our training materials they sell themselves.
1. **Enablement** because people can find detailed content about certain subjects.
1. **Awareness** because lots of time is spent on YouTube and our videos get lots of views in aggregate.
1. **Training** because content on YouTube is easier to consume even for team members, see [why not Google Drive](#why-not-google-drive)

## No quality bar

You don't need to be worried that something is not of the high enough quality because:

1. You can use the [GitLab Unfiltered YouTube account](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/) if the conversation is intended for team members only.
1. Algorithms will ensure that a video will be distributed to the right sized audience.
1. We can embed videos if and where they are relevant, for example in docs.
1. Enhance videos later if we need to ([trim it so it starts immediately](https://support.google.com/youtube/answer/9057455?hl=en), add transcript, extensive description, links to relevant materials)
1. Follow up later if we need to (rerecord, do an interview)

## No extra work

Producing videos shouldn't be extra work.
You should do what you normally do.
But when a meeting is possibly interesting for more people, make it a livestream.
Or when you start talking about something possibly relevant to more people, hit the record button.

## Make private quickly

In case there are any concerns raised about a video everyone has the authority to make it private.
You don't have to wait for any permission, just go to the channel specific YouTube Studio, for example [the one for unfiltered](https://studio.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/videos/upload) and mark the video private.
Please note that there is another tab for live videos.
In case you marked it private while it doesn't need to be it will be easy to undo it by marking the video public.

## Why not Google Drive

Always use YouTube and never use Google Drive, even for [private videos](#visibility), because YouTube videos:

1. are streamed [more reliably](https://peering.google.com/#/infrastructure).
1. have mouse-over thumbnails.
1. can be played at a higher speed.
1. can be fast forwarded and rewound in 10-second blocks.
1. can be timeshifted by adding them to a watch later list.
1. can be embedded, for example in the handbook.
1. restart at the right spot after being reloaded.
1. can be easily viewed on other devices, like TVs or streaming devices, with YouTube support.
1. allow links to a [specific time in the video](https://www.h3xed.com/web-and-internet/link-to-a-specific-time-in-a-youtube-video).
1. can have subtitles added automatically.
1. are [zero rated by some mobile providers](https://www.t-mobile.com/offer/binge-on-streaming-video.html)
1. will be served to people when it is relevant, automatically, since YouTube is a distribution channel.
1. allows anyone to contribute by leaving comments.
1. public videos show up in Google search.
1. easy to make public if the video is suitable for that.
1. they have playlists that you can use to organize them.

## Channels

There are two YouTube channels we use at GitLab:

1. [Main](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg): used for content intended for people in the wider community, for example [a conversation between two teammembers about multi-cloud](https://www.youtube.com/watch?v=maxXCbVlezw).
1. [Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/): used for content intended for teammembers, for example [a weekly meeting of the Plan group](https://www.youtube.com/watch?v=xXGAcFyBupA).

## Visibility

There are three types of visibility:

1. Main
2. Public
3. Private

The level of visibility should be displayed clearly in the titles of calendar invites.
For example when you do a livesteam to the public channel say: "Private stream" at the start of the invite.
You can skip live in livestream since every stream is live.
Do not say Unfiltered stream since it is ambiguous.
Do not say just livestream since people don't know what the level of access is.

We don't post:

1. Unlisted videos on any channel, this is too insecure for sensitive materials and not findable enough for public materials.
2. Private videos on our main channel, all videos here are for a wider audience.
3. Use Google Drive for any video, since it has [many drawbacks](#why-not-google-drive).

## Access

1. GitLab Filtered: To request access follow the instructions secret note 'YouTube Access' in the team vault in 1password.
1. GitLab Unfiltered: Everyone should get access to YouTube unfiltered during onboarding. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.

## Commenting

Remember that when you're signed into YouTube using an official GitLab account that any interaction you may have with other groups or people on the site (e.g. such as in the comments sections of videos) will be perceived as official communication from the company. In almost all cases, unless your job specifically requires you to interact in a community manager role or capacity, it is best to switch to a personal account prior to engaging with a video or user on YouTube.

## Organizing

Our YouTube channels contain a lot of content.
As a result it can often be hard to find videos once they are published.
Make liberal use of tags and playlists to organize video content you publish. 
For published recordings of recurring meetings, create a YouTube playlist.

## Starting a recorded video

The below instructions apply to either livestreams to YouTube or uploaded to YouTube videos.
When the meeting or call is about to start ask the host 'Can I initiate the countdown?' and wait for confirmation.
Count down from three (3, 2, 1).
Then click `Go Live!` or hit record, depending on your distribution medium. 
This ensures that there is no empty space at the beginning of the call.
The host should do an introduction stating who they are, what their role is, and what the meeting is about, e.g. "I am Jane Doe, the VP of Widget Production, and today's Group Conversation is about the molding phase of widget production."

## Live Streaming

### Why livestream?

We prefer livestreaming over recording and uploading because:

1. Allows more people to participate in real-time.
1. You don't have to upload the video to YouTube later on.
1. You can't forget about uploading the video.
1. It is clear to all participants that the content will be public.

But it can happen that you're not in a livestream and something interesting comes up.
In that case, you can [upload it to YouTube](#uploading-to-youtube).

### Considerations for livestreams
{:.no_toc}

1. Please remember to [follow the guidelines on starting a recorded video](#starting-a-recorded-video).
1. If you're not a host, you can't live stream direct from zoom, so the host will need to do that.
1. Generally, for a livestream, it's helpful to have a separate person handling [promoting people to panelist](https://support.zoom.us/hc/en-us/articles/115004834466-Managing-Participants-in-Webinar).


### Livestream with Zoom

1. Configure your Zoom [advanced meeting options](https://zoom.us/profile/setting#advanced_meeting_options) to enable livestreaming for youtube

![click the more button](/handbook/communication/youtube/zoom_livestream_settings.png)

1. Verify that there are no other livestreams happening at the same time as yours, and add the livestream to the GitLab Team Meeting calendar.

1. Click the \[ More ] button and click `Live on YouTube` 

![click the more button](/handbook/communication/youtube/zoom_live_on_youtube.png)

1. Select the `GitLab Unfiltered` or `GitLab`

![choose your account](/handbook/communication/youtube/choose_your_account.png)

1. Grant permissions to the Youtube account by clicking the `[Allow]` button

![grant permissions](/handbook/communication/youtube/grant_permissions.png)

1. Follow the instructions for [starting a recorded video](/handbook/communication/youtube/#starting-a-recorded-video).

![configure stream](/handbook/communication/youtube/stream_settings.png)

### Useful links

- [My Live Events: schedule and review upcoming live events](https://www.youtube.com/my_live_events)
- [YouTube live streaming introduction page](https://support.google.com/youtube/answer/2474026?hl=en) - Here you can check if your channel meets all requirements for live streaming
- [YouTube tutorial on setting up a live stream](https://support.google.com/youtube/answer/2853700?hl=en) - If you need extra help or information on setting up a youtube live stream in general
- [YouTube live dashboard](https://www.youtube.com/live_dashboard#) - Here you can find your encoder setup variables, manage your stream and see your chat

## Uploading conversations to YouTube

1. Default to [livestreaming](#live-streaming) instead of uploading. 1. Before you hit the record button please follow the process for [starting the recorded video](/handbook/communication/youtube/#starting-a-recorded-video).
1. Unless stated or arranged otherwise, our expected behavior is that the meeting organizer is responsible for distributing the recorded content after the meeting.
1. Log in to the [Zoom](https://zoom.us/) account of the meeting and go to the menu on the right and choose "My Recordings" (it can take up to 30 minutes before the recording is available to be shared).
1. Select the meeting and download the recording to your computer (if you can't find the recording because it was a while ago check "Trash" in the menu on the top left and "Recover" the recording).
1. Go to the [YouTube upload page](https://www.youtube.com/upload) and log into the [channel](#channels). If you're logged into your personal YouTube account, you may need to first log into your GitLab YouTube account to access the channel. If you're already logged into your GitLab YouTube account and have access to the channel, you need to switch accounts to that channel. To switch accounts, click on the account icon in the top right corner and then click "Switch account".
1. Drag and drop your recording into the window to upload it. Keep the privacy dropdown on the default 'Public' setting (unless there is confidential material). Use the dropdown menu to set the video to private if it the video contains confidential material.
1. While it's uploading, edit the title and description. Place "Confidential:" at the beginning of the video's title if the video will be kept private on our YouTube channel.
1. Be sure to include relevant links (for example a handbook page or presentation) in the description, and add the video to any relevant playlists.
1. When it is done uploading, press publish, then click on the Embed tab and copy the code, and insert that in the relevant part of the handbook or documentation.
1. After uploading a video to the GitLab Filtered channel, be sure to add a link to the new video in the #content-updates channel in Slack.

## Don't worry about the quality

1. There is no minimum quality, so please share it on our [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A?view_as=subscriber), as long as there is nothing inappropriate or confidential.
1. Everyone at the company probably has at least one conversation every week that is relevant to more people, so please share it.
1. We always list videos publicly instead of having them unlisted, unless there is confidential material. This allows more people to find the content. If the material is confidential, set the video to private.
1. Don't worry about whether or not it will be interesting to absolutely everyone. Just give it a descriptive title so people know what it is about, and let _them_ decide whether or not they should watch it.
1. Make sure that all participants are aware that you're recording.
1. You don't have to be sure it is interesting and OK to share when you start recording; you can make that decision after the fact.
1. If you record an in-person conversation with your mobile phone please hold your phone in landscape (horizontal) mode.

## Advanced setups

Depending on your needs, you might want use software to provide overlays and/or reroute audio.
99% of the people at GitLab don't use this, but see below for instructions if you do want to use it.

### Encoder
- [OBS Studio Win/Mac/Lin Open Source](https://obsproject.com/) or install with 'brew cask install obs' on mac

### Audio Rerouting
- [Soundflower Mac Open Source](https://github.com/mattingalls/Soundflower) or install with `brew cask install soundflower`
- [Loopback Mac Closed Source](http://www.rogueamoeba.com/loopback/) - Great closed source alternative
- [Windows alternatives](https://www.reddit.com/r/audioengineering/comments/3geqse/soundflower_alternative_on_windows/)
- [Linux alternatives](http://askubuntu.com/questions/602593/whats-a-good-soundflower-replacement-for-ubuntu)

### Setup

- Install both OBS Studio (encoder) and an audio rerouting software.
- [Check](https://support.google.com/youtube/answer/2474026?hl=en) if your channel is ready to live stream.
- Copy your encoder variables en setup your live streaming environment with the [YouTube live dashboard](https://www.youtube.com/live_dashboard#)
- Open up OBS Studio and open preferences
- Input your encoder variables under "Stream"

![obs studio preferences](/handbook/communication/youtube/OBS_Studio_Stream.jpeg)

- Configure your to-be-recorded software's audio to be rerouted to an alternative audio source/output.
- Under "Audio" in OBS Studio preferences, select the alternative audio source/output under Mic/Auxiliary Device 2. See [this page](http://code-zest.blogspot.nl/2016/02/setting-up-obs-with-audio-output-in-mac.html) for more help
- Under "Video" select your preferred resolution and fps settings. Please take note that these should optimally reflect video aspect ratio such as `1920x1080`.
- Optionally you can set up some output options under "Output"
- In the normal window of OBS Studio you should now see 2 audio sliders, including "Mic/Aux 2". Configure these to your liking.
- Select "Start Streaming": _You are now streaming_
- Go to your [YouTube live dashboard](https://www.youtube.com/live_dashboard#) to see your live stream and interact with your viewers.

## When to record and publish to YouTube

<iframe width="560" height="315" src="https://www.youtube.com/embed/RB8OC70RAfo?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
