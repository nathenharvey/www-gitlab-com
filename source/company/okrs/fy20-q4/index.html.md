---
layout: markdown_page
title: "FY20-Q4 OKRs"
---

This fiscal quarter will run from November 1, 2019 to January 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV
1. CRO: Build strong FY21 pipeline. FY21-Q1 pipeline at 2X target (stage 3+), FY21-Q2 pipeline at 1.2X target (stage 3+)
1. CRO: Ensure public company readiness. Sales & Marketing using a single pipeline dashboard, Conduct mock earnings call
1. VP of Engineering: [Enterprise-grade GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5566)
    1. Development: 
    1. Infrastucture: 
    1. Quality: 
    1. Security:
    1. Support: 
    1. UX: 

### 2. CEO: Popular next generation product

1. VP of Product Strategy: Get strategic thinking into the org. Secure [section strategy review](https://gitlab.com/gitlab-com/Product/issues/381), produce [strategy visuals](https://gitlab.com/gitlab-com/Product/issues/512).
1. VP of Product Strategy: Get acquisitions into shape; build a well-oiled machine. Build [soft-landing identification software](https://gitlab.com/gitlab-com/corporate-development/issues/1), identify 100 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).
1. VP of Product Strategy: Lay groundwork for strategic initiatives. Research 5 [strategic initiatives](https://docs.google.com/document/d/1CBvC5iSgvSfn0oBAu3ph9MAzrMHxhKCSjvigz0Lm1VM/edit).
1. VP of Product Management: Deliver on product roadmap.  Achieve at least 70% of our [category maturity](/direction/maturity/) plans.
1. VP of Product Management: Proactively validate problems and solutions with customers.  At least 2 [validation cycles](/handbook/product-development-flow/#validation-track) completed per Product Manager.
1. VP of Product Management: Create stage level product demos to build customer empathy.  Deliver one recorded demo for each stage.
1. Director of Product, Growth: Establish a rapid cadence of testing & learning.  At least one deliverable per week, per growth group.
1. VP of Engineering: [TBD](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5564)
    1. Development: 
    1. Infrastucture: 
    1. Quality: 
    1. Security:
    1. Support: 
    1. UX: 

### 3. CEO: Great team

1. VP of Engineering: [Provide mentorship/coaching at every level](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5565)
    1. Development: 
    1. Infrastucture: 
    1. Quality: 
    1. Security:
    1. Support: 
    1. UX: 
