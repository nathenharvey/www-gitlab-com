---
layout: job_family_page
title: "People Operations Analyst"
---

## People Analyst Coordinator 

## Responsibilites 

- HRIS data entry. 
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date  
  - Coordinate any changes with payroll
- Collaborate with the People Ops team on People Operations policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Keep it efficient and DRY.

## Intermediate People Operations Analyst

### Responsibilities

- Coordinate on compensation strategies and principles.
- Participate in compensation and benefits surveys as directed.
- HRIS data entry.
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date  
  - Coordinate any changes with payroll
- Collaborate with the People Ops team on People Operations policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Coordinate on benefits administration.
  - Audit and pay all US benefit invoices.
- Collect data to track trends in functional areas.
- Assist with training employees on various topics.
- Maintain approval requests in Greenhouse ensuring a job family and compensation benchmark are set.
- Keep it efficient and DRY.

### Requirements

- 2-5 years experience in an HR or People Operations role with a concentration on compensation and benefits  
- Bachelor's degree in Mathematics, Business, or HR preferred
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools 
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Desire to work for a fast moving startup
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).


## Senior People Operations Analyst

### Responsibilities

- Develop compensation strategies and principles.
- HRIS data entry, ensuring Data Integrity and alignment with all ancillary systems.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Maintain people operations documentation.
- Benefits management.
- Collect and analyze data to track trends in functional areas.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Collaborate on training employees on various topics.
- Keep it efficient and DRY.

## Compensation & Benefits Manager

### Responsibilities

- Manage and implement global compensation strategies and principles.
- Global benefits management and development of benefit principles.
- Manage the design and development of tools to assist employees in benefits selection, and to guide managers through compensation decisions.
- Plan, direct, supervise, and coordinate work activities of direct reports relating to compensation, benefits, and people operations data.
- Mediate between benefits providers and employees, such as by assisting in handling employees' benefits-related questions or taking suggestions.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Audit all systems to ensure data accuracy.
- Develop and implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Manage people operations documentation.
- Iterate based on data trend findings.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Keep it efficient and DRY.

## Performance Indicators

Primary performance indicators for this role:

- [Percentage over compensation band](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](https://about.gitlab.com/handbook/hiring/metrics/#new-hire-location-factor)

