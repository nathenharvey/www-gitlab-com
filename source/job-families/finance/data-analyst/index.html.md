---
layout: job_family_page
title: "Data Analyst"
---

## Responsibilities

* Expand our data warehouse with clean data, ready for analysis
* Understand and document the full lifecycle of data from numerous sources and how to model it for easy analysis
* Build reports and dashboards to help teams identify opportunities and explain trends across data sources
* Follow and improve our processes for maintaining high quality data and reporting
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Collaborate with other functions to create useful analyses and democratize insights across the company
* Build upon and document our common data framework so that all data can be connected and analyzed
* This position reports to the Manager, Data

## Requirements

* 2+ years experience in an analytics role
* Deep understanding of SQL and analytical data warehouses (we use Snowflake)
* Hands on experience working with Python and SQL to generate business insights and drive better organizational decision making
* Experience building reports and dashboards in a data visualization tool
* Familiarity with git and the command line
* Passionate about data, analytics and automation. Experience cleaning and modeling large quantities of raw, disorganized data (we use dbt)
* Experience with a variety of data sources. Our data includes Salesforce, Zuora, Zendesk, Marketo, NetSuite, Snowplow and many others (see the [data team page](https://about.gitlab.com/handbook/business-ops/data-team/#-extract-and-load))
* Share and work in accordance with our values
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)

## Senior Requirements
All of the responsibilities of a Data Analyst, plus:
* Advocate for improvements to analysis quality, security, and performance that have particular impact across your team and the organization
* Solve technical problems of high scope and complexity
* Exert influence on the overall objectives and long-range goals of your team
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions
* Provide mentorship for Junior and Intermediate Analysts on your team to help them grow in their technical responsibilities and remove blockers to their autonomy
* Confidently ship moderately sized analyses and transformation models with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Build close relationships with other functional teams to truly democratize data understanding and access

## Intern Requirements
An intern is not required to meet the standards of an intermediate data analyst but she or he is required to be interested in developing in towards them.
An intern must:
* Have a track record of asking hard questions and thinking critically
* Self-starter committed to remote work and its intricacies
* Proactive, positive, energetic, customer service personality
* Ability to articulate in a clear, concise manner, disseminating complete and accurate information
* Ability to deal effectively with people of multi-cultural societies
* Attention to detail
* Strong organizational skills

### What you'll do
* Work with and learn from a talented team of data professionals
* Develop and execute an independent project under direct mentorship
* Write blog posts about your learnings
* Update, maintain, and coordinate meetings
* Update the handbook using git and GitLab
* Identify Data team process weaknesses and blindspots
* Contribute fresh perspective and speak up where you can add value

### How you'll ramp

#### By the 30 day mark...
* Helping lead definition-related conversations, acting as thought-leader to functional group
* Working independently in Periscope, writing SQL queries to produce dashboards
* Clear sense of where prioritization comes from
* Understands values/working the GitLab way
* Comfortable participating in triage rotation without a named backup

#### By the 60 day...
* Comfortable working with dbt
* Comfortable participating in triage rotation on own
* Contributing to internal conversations on data organization and structure

#### By the 90 day...
* Comfortable building a new data source from scratch
* Can direct team members to answers in the handbook/in Periscope
* Regularly contributing to documentation and housekeeping improvements for the team 
* Can gather requirements, scope, and build analysis with little-to-no guidance from more senior analysts

## Specialties

### Engineering
* Support all departments in the engineering division by helping drive the standardization, capture, automation, and implementation of performance indicators
* Be intimately familiar with productivity metrics
* Priorities will be set by the VP, Enfineering but will collaborate with and reporting into the Data Team

### Finance
* Support the FP&A team in driving financial and operational initiatives by analyzing data and discovering insights
* Focus on financial and operational specific data
* Priorities will be set by the Manager, Financial Planning and Analysis but will collaborate with and report into the Data Team
* Spend 80% of time supporting the FP&A team and spend the remaining 20% of time contributing to the Data Team
* The Manager, Financial Planning and Analysis will evaluate the analyst on 80% of the goals in the experience factor worksheet relating to supporting FP&A and the Manager, Data will evaluate the analyst on the remaining 20% of goals relating to supporting the Data Team

### Growth
* Support the product management function in driving product growth, reducing churn, increasing user engagement by analyzing data and discovering insights
* Focus on product-specific data - usage ping, SaaS DB, Snowplow events
* Priorities will be set by a Product Manager, Growth but will collaborate with and report into the Data Team

### Product
* Support the Product function by spearheading tracking and reporting initiatives
* Focus on product usage metrics across SaaS and self-managed products
* Build cross-functional analyses to drive strategic decision-making 
* Priorities will be set by a Director of Product but will collaborate with and report into the Data Team

### Sales
* Coordinate with SalesOps to improve and automate tracking potentially insightful data points
* Focus on cross-functional analyses that can help drive sales conversations (e.g. product usage into renewal conversations)
* Priorities will be set by the sales function but will collaborate with and report into the Data Team

### Infrastructure

The **Infrastructure Department** is the primary responsible party for the **availability**, **reliability**, **performance**, and **scalability** of all user-facing services (most notably **GitLab.com**). Other departments and teams contribute greatly to these attributes of our service as well. In these cases it is the responsibility of the Infrastructure Department to close the feedback loop with monitoring and metrics to drive accountability.

The Data Analyst, Infrastructure is a key member of the Infrastructure team and works to enhance and improve our business operations and our forecasting and financial modeling capabilities, developing sound business practices within the Infrastructure Department to guide infrastructure resource utilization and associated costs optimizations over time, as well as tracking and modeling of all relevant KPIs.

#### Data Analyst, Infrastructure Responsibilities

- Develop infrastructure resource utilization and forecasting models to meet business and financial objectives
- Develop infrastructure financial models to provide data-driven guidance on cost decisions
- Interface with Finance, Legal and vendors to manage Infrastructure-related contracts
- Interface with Sales to understand sales pipeline and its effect on Infrastructure
- Develop and maintain Infrastructure-centric sales collateral as it relates to GitLab.com
- Manage service level framework (SLIs, SLOs, SLAs) and associated error budgets to meet business objectives for GitLab.com
- Manage Infrastructure's [Performance Indicators](handbook/engineering/infrastructure/performance-indicators/)
- Review Infrastructure business processes and policies and help enhance workflows in support of GitLab.com and Infrastructure-provided services to the rest of the company
- Develop a deep understanding of the infrastructure vendor landscape to help Infrastructure leaders select, work and optimize infrastructure usage

The Data Analyst, Infrastructure reports to the Director of Engineering, Infrastructure.

#### Data Analyst, Infrastructure Performance Indicators

* [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
* [Infrastructure cost vs plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)

## Performance Indicators (PI)

*  [Adoption of Data Team BI charts throughout company](/handbook/business-ops/metrics/#adoption-of-data-team-bi-charts-throughout-company)
*  [% of issues requested triaged with first response within 36 hours (per business unit)](/handbook/business-ops/metrics/#percent--of-issues-requested-triaged-with-first-response-within-36-hours-per-business-unit)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to fill out a short questionnaire.
* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Analyst
* Next, candidates will be invited to schedule a third interview with our Dir. of Business Operations
* Next, if applying for a specialty, candidates will be invited to schedule a fourth interview with our the specialty lead
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
